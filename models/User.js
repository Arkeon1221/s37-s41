const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email name is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String, 
		required: [true, 'Mobile number is required.']
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Couse ID is required.']
			},	
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Enrolled'
			}	
			
		}
	]
})

module.exports = mongoose.model('User', user_schema)